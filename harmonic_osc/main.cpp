#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <string>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
using namespace std;

/* Computation of the integral,
*/ 

struct my_f_params { double a; double m; double start_and_end_pnt; int N;};

double lagrnge(double fac, double fac2, double k1, double k2){
  return fac * pow(k2-k1, 2) + fac2 * pow(k1, 2)/2.0;
}

double g (double *k, size_t dim, void *params)
{
  struct my_f_params * fp = (struct my_f_params *)params;
  double fac = fp->m/2.0/fp->a;
  double fac2 = fp->a;
  double A = pow(fp->m/2.0/M_PI/fp->a, (double)fp->N/2.0);
  double s = 0.0;

  // add first end point
  s += lagrnge(fac, fac2, fp->start_and_end_pnt, k[0]);
  
  // add 2nd end point
  s += lagrnge(fac, fac2, k[dim-1], fp->start_and_end_pnt);

  // include points between 1 and N-1
  for(int i=0;i<dim-2;i++){
    s += lagrnge(fac, fac2, k[i], k[i+1]);
  }
  return A*exp(-s);
}

void display_results (string title, double result, double error, double exact)
{
  cout << title << "==================\n" << endl;
  printf ("result = % .6f\n", result);
  printf ("sigma  = % .6f\n", error);
  printf ("exact  = % .6f\n", exact);
  printf ("error  = % .6f = %.2g sigma\n", result - exact,
          fabs (result - exact) / error);
}

// pass in value of x from command line
int main (int argc, char *argv[])
{
  double Time = 4.0;
  int N = 8;
  double a = Time/(float)N;
  double m = 0.5;
  size_t ndim = N-1; // N-1
  double x = atof(argv[1]); // only one value of x
  double start_and_end_pnt = x; // want propagator of staying at x
  double Eo = 1.0/2.0;
  double wfc_val = exp(-pow(x, 2)/2.0)/pow(M_PI, 0.25);
  double exact = pow(wfc_val, 2)*exp(-Eo*Time); // Assuming wfc is REAL!!!!!
  double res, err;
  double xl[ndim];
  double xu[ndim];
  struct my_f_params params = {a, m, start_and_end_pnt, N};

  const gsl_rng_type *T;
  gsl_rng *r;

  for(int i=0;i<ndim;i++){
    xl[i] = -5.0;
    xu[i] = 5.0;
  }

  gsl_monte_function G = { &g, ndim, &params };

  size_t calls = 6000000;

  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  gsl_monte_plain_state *s = gsl_monte_plain_alloc (ndim);
  gsl_monte_plain_integrate (&G, xl, xu, ndim, calls, r, s, 
                             &res, &err);
  gsl_monte_plain_free (s);

  //display_results ("plain", res, err, exact);
  cout<<res<<endl;

  gsl_rng_free (r);

  return 0;
}
