make -s clean 
make -s
echo "#x #val" > plot.dat
for i in $(seq 0 0.2 2.0)
do
  rslt=`./main $i`
  echo $i $rslt >> plot.dat
done
gnuplot -e "set term png; set output 'plot.png'; plot 'plot.dat' using 1:2"
